from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32

from visualization_msgs.msg import Marker
import numpy as np
import builtin_interfaces.msg
from geometry_msgs.msg import Point

from builtin_interfaces.msg import Time
from builtin_interfaces.msg import Duration

from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from squaternion import Quaternion


class PolygonPointGenerator(object):
    def __init__(self, dx, dy, dz, length, width, heading,
                 id, frame_id, sec, nsec, object_type):
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.length = length
        self.width = width

        self.yaw_rad = heading
        self.yaw_degree = heading / 0.0174532925

        add = 1
        if object_type == 1:
            add = 0
        if object_type == 6:
            add = 5000
        if object_type == 5:
            add = 10000

        self.id = id + add
        self.frame_id = frame_id
        self.sec = sec
        self.nsec = nsec


    def calculatePolygonPointCoor(self, length, yaw_rad):
        center_x = self.dx
        center_y = self.dy
        center_z = self.dz
        end_x = center_x + np.cos(yaw_rad) * (length)
        end_y = center_y + np.sin(yaw_rad) * (length)
        end_z = center_z
        return [end_x, end_y, end_z]

    def giveMarkerPolygonPointsANDPolygon(self):
        list_points_coor = []

        length_arrow_forward = self.length / 2
        diff_degree = np.arctan((self.width / 2) / (self.length / 2)) * 57.2957795
        length_vec_left_forward = np.sqrt(pow(self.length / 2, 2) + pow(self.width / 2, 2))
        length_vec_left = self.width / 2
        length_vec_left_back = length_vec_left_forward
        length_vec_back = self.length / 2
        length_vec_right_back = length_vec_left_forward
        length_vec_right = self.width / 2
        length_vec_right_forward = length_vec_left_forward

        # Right back:
        right_back = self.calculatePolygonPointCoor(length_vec_right_back,
                                                    self.yaw_rad + ((180 + diff_degree) * 0.0174532925))

        right = self.calculatePolygonPointCoor(length_vec_right,
                                               self.yaw_rad - (90 * 0.0174532925))
        right_forward = self.calculatePolygonPointCoor(length_vec_right_forward,
                                                       self.yaw_rad - (diff_degree * 0.0174532925))
        forward = self.calculatePolygonPointCoor(length_arrow_forward, self.yaw_rad)
        left_forward = self.calculatePolygonPointCoor(length_vec_left_forward,
                                                      self.yaw_rad + (diff_degree * 0.0174532925))
        left = self.calculatePolygonPointCoor(length_vec_left,
                                              self.yaw_rad + (90 * 0.0174532925))
        left_back = self.calculatePolygonPointCoor(length_vec_left_back,
                                                   self.yaw_rad + ((180 - diff_degree) * 0.0174532925))
        back = self.calculatePolygonPointCoor(length_vec_back,
                                              self.yaw_rad + (180 * 0.0174532925))

        list_points_coor.append(right_back)
        #list_points_coor.append(right)
        list_points_coor.append(right_forward)
        #list_points_coor.append(forward)
        list_points_coor.append(left_forward)
        #list_points_coor.append(left)
        list_points_coor.append(left_back)
        #list_points_coor.append(back)

        marker_points = self.giveMarkerPoints(list_points_coor, self.id)
        list_polygon_points = self.generatePolygonPoints(list_points_coor)
        return marker_points, list_polygon_points

    def giveMarkerPoints(self, list_point_coor, id):
        marker = Marker()
        marker.header.frame_id = self.frame_id
        timestamp = Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.type = Marker.POINTS
        marker.action = Marker.ADD
        marker.id = id
        marker.ns = "marker_polygon_points"
        marker.lifetime = Duration(sec=0, nanosec=100000000)
        marker.scale.x = 0.3
        marker.scale.y = 0.3
        marker.scale.z = 0.3
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 1.0

        for point_coor in list_point_coor:
            point = Point()
            point.x = point_coor[0]
            point.y = point_coor[1]
            point.z = point_coor[2]
            marker.points.append(point)

        return marker

    def generatePolygonPoints(self,
                              list_point_coor):
        list_polygon_points = []
        for point_coor in list_point_coor:
            point = Point32()
            point.x = point_coor[0]
            point.y = point_coor[1]
            point.z = point_coor[2]
            list_polygon_points.append(point)
        return list_polygon_points
