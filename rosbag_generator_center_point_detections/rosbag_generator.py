import rosbag2_py
from rclpy.serialization import deserialize_message, serialize_message
from rosidl_runtime_py.utilities import get_message
import os
import sys
from waymo_open_dataset.protos import metrics_pb2
from waymo_open_dataset import label_pb2
from os import listdir
from os.path import isfile, join
from Helper import Helper
from tqdm import tqdm


def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def create_topic(writer, topic_name, topic_type, serialization_format='cdr'):
    """
    Create a new topic.
    :param writer: writer instance
    :param topic_name:
    :param topic_type:
    :param serialization_format:
    :return:
    """
    topic_name = topic_name
    topic = rosbag2_py.TopicMetadata(name=topic_name, type=topic_type,
                                     serialization_format=serialization_format)
    writer.create_topic(topic)


if __name__ == '__main__':
    path_tf_folder = sys.argv[1]  # '/home/goktug/Desktop/Waymo_Valid_Dataset/valid0/validation_validation_0000'
    path_center_point_detections = sys.argv[2]
    output_folder_path = sys.argv[3]  # '/home/goktug/ros2bags'

    list_name_tfrecords = [f for f in listdir(path_tf_folder) if isfile(join(path_tf_folder, f))]

    # Read center point detections:
    f = open(path_center_point_detections, 'rb')
    objects = metrics_pb2.Objects()
    objects.ParseFromString(f.read())
    list_context_names = [item.split('.')[0].split('-')[1].split('_with')[0] for item in list_name_tfrecords]
    dict_center_point_detections = {}
    for context_name in list_context_names:
        dict_center_point_detections[context_name] = []
    for context_name in tqdm(list_context_names):
        for object_ in objects.objects:
            if object_.context_name == context_name:
                #print("Object is found!")
                dict_center_point_detections[context_name].append(object_)
    f.close()

    # Iterate over tf paths:
    for id, tf_name in enumerate(list_name_tfrecords):
        path_ros2bag = output_folder_path + '/' + tf_name.split('.')[0]
        path_tfrecord = path_tf_folder + '/' + tf_name
        context_name = tf_name.split('.')[0].split('-')[1].split('_with')[0]
        # Read tfrecord:
        helper = Helper(path_tfrecord, dict_center_point_detections[context_name])

        list_frame_by_frame_lidar_labels_gt, \
        list_frame_by_frame_trans, \
        list_frame_by_frame_odometry, list_frame_by_frame_bbox_array_gt, \
        list_frame_by_frame_lidar_labels_centerpoint, \
        list_frame_by_frame_bbox_array_centerpoint, \
        frame_count = helper.readDataset()

        storage_options, converter_options = get_rosbag_options(path_ros2bag)
        writer = rosbag2_py.SequentialWriter()
        writer.open(storage_options, converter_options)

        # Iterate over frames:
        for frame_id in range(frame_count):
            single_frame_lidar_labels_gt = list_frame_by_frame_lidar_labels_gt[frame_id]
            single_frame_trans = list_frame_by_frame_trans[frame_id]
            single_frame_odometry = list_frame_by_frame_odometry[frame_id]
            single_frame_bbox_array_gt = list_frame_by_frame_bbox_array_gt[frame_id]
            single_frame_lidar_labels_centerpoint = list_frame_by_frame_lidar_labels_centerpoint[frame_id]
            single_frame_bbox_array_centerpoint = list_frame_by_frame_bbox_array_centerpoint[frame_id]

            # 0: msg, 1: topic_name, 2: time_stamp
            create_topic(writer, single_frame_lidar_labels_gt[1], 'visualization_msgs/msg/MarkerArray')
            writer.write(single_frame_lidar_labels_gt[1],
                         serialize_message(single_frame_lidar_labels_gt[0]),
                         single_frame_lidar_labels_gt[2])

            create_topic(writer, single_frame_lidar_labels_centerpoint[1], 'visualization_msgs/msg/MarkerArray')
            writer.write(single_frame_lidar_labels_centerpoint[1],
                         serialize_message(single_frame_lidar_labels_centerpoint[0]),
                         single_frame_lidar_labels_centerpoint[2])

            # Write tf msgs:
            create_topic(writer, single_frame_trans[1], 'tf2_msgs/msg/TFMessage')
            writer.write(single_frame_trans[1],
                         serialize_message(single_frame_trans[0]),
                         single_frame_trans[2])

            # Write odometry in map frame:
            create_topic(writer, single_frame_odometry[1], 'nav_msgs/msg/Odometry')
            writer.write(single_frame_odometry[1],
                         serialize_message(single_frame_odometry[0]),
                         single_frame_odometry[2])
            # Write bbox array:
            create_topic(writer, single_frame_bbox_array_gt[1], 'autoware_auto_msgs/msg/BoundingBoxArray')
            writer.write(single_frame_bbox_array_gt[1],
                         serialize_message(single_frame_bbox_array_gt[0]),
                         single_frame_bbox_array_gt[2])

        # break
        print(path_ros2bag, " DONE!")
        print('************************************************************************************************')

    print("All files are extracted.")
