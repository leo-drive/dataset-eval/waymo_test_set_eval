import multiprocessing
import time
import os
import signal
from os import walk

import numpy as np
from waymo_open_dataset.protos import metrics_pb2


class Multiprocessing(object):
    def __init__(self, node_path, path_baseline_det_vehicle, path_baseline_det_ped,
                 path_baseline_det_cyc, path_tfrecords_folder, path_tf_file,
                 path_rosbag_folder, path_output_file):
        # self.tracker_node_pid = 0
        self.prediction_generator_node = multiprocessing.Process(name='worker1',
                                                                 target=self.function_prediction_generator_node,
                                                                 args=(node_path, path_baseline_det_vehicle,
                                                                       path_baseline_det_ped,
                                                                       path_baseline_det_cyc, path_tfrecords_folder,
                                                                       path_tf_file, path_output_file))
        # self.tracker_node = multiprocessing.Process(name='worker2', target=self.function_tracker_node)
        self.rosbag_play = multiprocessing.Process(name='worker3', target=self.function_rosbag_play,
                                                   args=(path_tf_file, path_rosbag_folder))

        self.prediction_generator_node.start()
        # self.tracker_node.start()
        time.sleep(25)
        self.rosbag_play.start()

        self.prediction_generator_node.join()
        # self.tracker_node.join()
        self.rosbag_play.join()
        time.sleep(5)

        print("All processes are killed, single segment predictions is appended to binary file!")

    def function_prediction_generator_node(self, node_path, path_baseline_det_vehicle, path_baseline_det_ped,
                                           path_baseline_det_cyc, path_tfrecords_folder, path_tf_file,
                                           path_output_file):
        os.system("python3" + ' ' + node_path + ' ' + path_baseline_det_vehicle + ' ' +
                  path_baseline_det_ped + ' ' +
                  path_baseline_det_cyc + ' ' +
                  path_tfrecords_folder + ' ' +
                  path_tf_file + ' ' + path_output_file)

    def function_tracker_node(self):
        self.tracker_node = os.getpid()
        os.system("ros2 run tracking_nodes multi_object_tracker_node_exe "
                  "--ros-args --params-file /home/goktug/projects/AutowareAuto"
                  "/src/perception/tracking_nodes/param/test.param.yaml")

    def function_rosbag_play(self, path_tf_file, path_rosbag_folder):
        rosbag_name = path_tf_file.split('.')[0] + '_0.db3'
        redundant_folder_name = path_tf_file.split('.')[0]
        path_rosbag = path_rosbag_folder + '/' + rosbag_name
        os.system("ros2 bag play " + path_rosbag + "")

def health_checker(path_predcition_file):
    list_set_context_names = []
    f = open(path_predcition_file, 'rb')
    objects = metrics_pb2.Objects()
    objects.ParseFromString(f.read())
    for object in objects.objects:
        if object.context_name not in list_set_context_names:
            list_set_context_names.append(object.context_name)
    #print(list_set_context_names)

    f.close()
    return list_set_context_names


if __name__ == '__main__':
    node_path = '/home/goktug/projects/waymo_test_set_eval/waymo_prediction_generator/waymo_prediction_generator/waymo_prediction_generator_node.py'
    path_baseline_det_vehicle = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_pedestrian_detection_validation.bin'
    path_baseline_det_ped = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_vehicle_detection_validation.bin'
    path_baseline_det_cyc = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_cyclist_detection_validation.bin'
    path_tfrecords_folder = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/tfrecords'
    path_rosbag_folder = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/rosbags'
    path_output_file = '/home/goktug/Desktop/pred_LeoDrive_3d_multi_object_tracker.bin'
    # path_tf_file = 'segment-2601205676330128831_4880_000_4900_000_with_camera_labels.tfrecord'

    list_tf_files = next(walk(path_tfrecords_folder), (None, None, []))[2]
    list_set_context_names_before = health_checker(path_output_file)

    for path_tf_file in list_tf_files:

        context_name_current = path_tf_file.split('_with')[0].split('-')[1]
        if context_name_current in list_set_context_names_before:
            print("This segment is already exists!")
            continue

        multiprocessing_ = Multiprocessing(node_path, path_baseline_det_vehicle,
                                           path_baseline_det_ped, path_baseline_det_cyc,
                                           path_tfrecords_folder, path_tf_file, path_rosbag_folder,
                                           path_output_file)

        list_set_context_names = health_checker(path_output_file)
        print("########################################################")
        print("Prediction file contains those segment evaluations:")
        print(np.array(list_set_context_names))
        print(len(list_set_context_names))

        print("########################################################")

        del multiprocessing_
        time.sleep(2)
        print("Next segment...")

    print("Prediction file generation is done!")
