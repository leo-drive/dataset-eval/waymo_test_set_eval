from os import listdir
from os.path import isfile, join
import numpy as np
import os
import tensorflow.compat.v1 as tf

# tf.enable_eager_execution()
from waymo_open_dataset import dataset_pb2 as open_dataset
from autoware_auto_msgs.msg import TrackedObject
from autoware_auto_msgs.msg import TrackedObjects
from autoware_auto_msgs.msg import ObjectClassification
from autoware_auto_msgs.msg import Shape
from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32
from geometry_msgs.msg import PoseWithCovariance
from squaternion import Quaternion
import math
from visualization_msgs.msg import Marker
import builtin_interfaces.msg
from geometry_msgs.msg import Point

# Alternative for tf py
from squaternion import Quaternion
from autoware_auto_msgs.msg import TrackedObject
from autoware_auto_msgs.msg import TrackedObjects
from waymo_open_dataset.protos import metrics_pb2


class Helper(object):
    def __init__(self, path_tf_record,
                 path_gt_detection_vehicle,
                 path_gt_detection_pedestrian,
                 path_gt_detection_cyclist):

        self.path_tf_record = path_tf_record
        self.context_name = self.path_tf_record.split('/')[-1].split('-')[-1].split('.')[0].split('_with')[0]
        self.path_gt_detection_vehicle = path_gt_detection_vehicle
        self.path_gt_detection_pedestrian = path_gt_detection_pedestrian
        self.path_gt_detection_cyclist = path_gt_detection_cyclist

        self.list_timestamp_waymo = []
        self.list_timestamp_ros = []
        self.frame_count_current_dataset = 0
        self.dataset = 0

        self.prob_threshold = 0.2

        self.list_baseline_detections_all = []
        self.dict_baseline_detections_all_frames = {}

        self.dict_gt_obj_all_frames = {}

    def read_baseline_detection_files(self):
        # Read ground truth detections in the related segment - VEHICLE:
        f = open(self.path_gt_detection_vehicle, 'rb')
        objects = metrics_pb2.Objects()
        objects.ParseFromString(f.read())
        for object_ in objects.objects:
            if object_.context_name == self.context_name:
                if object_.score < self.prob_threshold:
                    continue
                self.list_baseline_detections_all.append(object_)
        f.close()
        # Read ground truth detections in the related segment - PEDESTRIAN:
        f = open(self.path_gt_detection_pedestrian, 'rb')
        objects = metrics_pb2.Objects()
        objects.ParseFromString(f.read())
        for object_ in objects.objects:
            if object_.context_name == self.context_name:
                if object_.score < self.prob_threshold:
                    continue
                self.list_baseline_detections_all.append(object_)
        f.close()
        # Read ground truth detections in the related segment - CYCLIST:
        f = open(self.path_gt_detection_cyclist, 'rb')
        objects = metrics_pb2.Objects()
        objects.ParseFromString(f.read())
        for object_ in objects.objects:
            if object_.context_name == self.context_name:
                if object_.score < self.prob_threshold:
                    continue
                self.list_baseline_detections_all.append(object_)
        f.close()

    def read_tfrecord(self):
        self.dataset = tf.data.TFRecordDataset(self.path_tf_record, compression_type='')
        frame_counter = 0
        for frame_id, data in enumerate(self.dataset):
            # print("Frame id:", frame_id)
            frame_counter = frame_id + 1
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            self.context_name = frame.context.name
            self.list_timestamp_waymo.append(frame.timestamp_micros)
            frame_waymo_time = frame.timestamp_micros
            frame_stamp_secs = int(frame.timestamp_micros / 1000000)
            frame_stamp_nsecs = int((frame.timestamp_micros / 1000000.0 - frame_stamp_secs) * 1000000000)
            time_stamp_ros = (frame_stamp_secs, frame_stamp_nsecs)
            self.list_timestamp_ros.append(time_stamp_ros)

            liste = []
            self.dict_gt_obj_all_frames[frame_id] = liste
            # Iterate over ground truths in a single frame:
            for index, laser_label in enumerate(frame.laser_labels):
                self.dict_gt_obj_all_frames[frame_id].append(laser_label)

            liste = []
            self.dict_baseline_detections_all_frames[frame_id] = liste

            # Iterate over the whole baseline detections:
            counter_veh = 0
            counter_ped = 0
            counter_cyc = 0
            for baseline_object in self.list_baseline_detections_all:
                if baseline_object.frame_timestamp_micros == frame_waymo_time:
                    self.dict_baseline_detections_all_frames[frame_id].append(baseline_object)
                    if baseline_object.object.type == 1:
                        counter_veh += 1
                    if baseline_object.object.type == 2:
                        counter_ped += 1
                    if baseline_object.object.type == 4:
                        counter_cyc += 1

            # print("Vehicle pedestrian cyclist counts:", counter_veh, counter_ped, counter_cyc)
            # print("#######################################################################")

        self.frame_count_current_dataset = frame_counter
        print("Context name:", self.context_name)
        print("Frame count in tfrecord file:", self.frame_count_current_dataset)
        print("ALL FILES ARE READ!")

    def get_dataset_timestamp(self, secs, nsecs):
        for i in range(self.frame_count_current_dataset):
            (ros_secs, ros_nsecs) = self.list_timestamp_ros[i]
            if (ros_secs == secs and ros_nsecs == nsecs):
                # print("Dataset time: ", self.list_timestamp_waymo[i],"(microseconds)")
                return i, self.list_timestamp_waymo[i], self.context_name, self.frame_count_current_dataset

    def give_tracked_object_info(self, tracked_object):
        # tracked_object = TrackedObject()
        center_x = tracked_object.kinematics.pose.pose.position.x
        center_y = tracked_object.kinematics.pose.pose.position.y
        center_z = tracked_object.kinematics.pose.pose.position.z
        roll_x, pitch_y, yaw_z = self.euler_from_quaternion(tracked_object.kinematics.pose.pose.orientation.x,
                                                            tracked_object.kinematics.pose.pose.orientation.y,
                                                            tracked_object.kinematics.pose.pose.orientation.z,
                                                            tracked_object.kinematics.pose.pose.orientation.w)
        heading = yaw_z

        # Determine length width height of bbox:
        height = tracked_object.height
        length = tracked_object.length
        width = tracked_object.width

        # print("Type: ", tracked_object.classification[0].classification)

        global type
        if tracked_object.classification[0].classification == 1:
            type = 1
            #print("vehicle")
        elif tracked_object.classification[0].classification == 6:
            type = 2
            #print("ped")
        elif tracked_object.classification[0].classification == 5:
            type = 4
            #print("cyclist")

        # print("height: ", height, "length: ", length, "width:", width)
        return center_x, center_y, center_z, heading, height, length, width, str(tracked_object.object_id), type

    def euler_from_quaternion(self, x, y, z, w):
        """
        Convert a quaternion into euler angles (roll, pitch, yaw)
        roll is rotation around x in radians (counterclockwise)
        pitch is rotation around y in radians (counterclockwise)
        yaw is rotation around z in radians (counterclockwise)
        """
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)

        return roll_x, pitch_y, yaw_z  # in radians

    def distance(self, p1, p2):
        return math.sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2))

    def distance3D_baseline(self, gt_o, tracked_object):
        return math.sqrt(pow(gt_o.object.box.center_x - tracked_object.kinematics.pose.pose.position.x, 2) +
                         pow(gt_o.object.box.center_y - tracked_object.kinematics.pose.pose.position.y, 2) +
                         pow(gt_o.object.box.center_z - tracked_object.kinematics.pose.pose.position.z, 2))

    def distance3D_gt(self, gt_o, tracked_object):
        return math.sqrt(pow(gt_o.box.center_x - tracked_object.kinematics.pose.pose.position.x, 2) +
                         pow(gt_o.box.center_y - tracked_object.kinematics.pose.pose.position.y, 2) +
                         pow(gt_o.box.center_z - tracked_object.kinematics.pose.pose.position.z, 2))


    def get_closest_objects(self, frame_id, tracked_object):
        type = 0
        if tracked_object.classification[0].classification == 1:
            type = 1
            #print("vehicle")
        elif tracked_object.classification[0].classification == 6:
            type = 2
            #print("ped")
        elif tracked_object.classification[0].classification == 5:
            type = 4
            #print("cyclist")

        min_dist = 999999999
        closest_obj_baseline = 1
        for baseline_obj in self.dict_baseline_detections_all_frames[frame_id]:
            '''if type != baseline_obj.object.type:
                continue'''
            dist = self.distance3D_baseline(baseline_obj, tracked_object)
            if dist < min_dist:
                min_dist = dist
                closest_obj_baseline = baseline_obj

        min_dist = 999999999
        closest_obj_gt = 1
        for gt_obj in self.dict_gt_obj_all_frames[frame_id]:
            '''if type != gt_obj.type:
                continue'''
            dist = self.distance3D_gt(gt_obj, tracked_object)
            if dist < min_dist:
                min_dist = dist
                closest_obj_gt = gt_obj

        return closest_obj_baseline, closest_obj_gt

    def get_gt_object_count(self, frame_id):
        return len(self.dict_gt_obj_all_frames[frame_id])


class Visualizer(object):
    def __init__(self, center_x, center_y,
                 center_z, heading,
                 height, width,
                 length, object_id,
                 type,
                 sec,
                 nsec,
                 tracked_object):
        self.center_x = center_x
        self.center_y = center_y
        self.center_z = center_z
        self.heading = heading
        self.height = height
        self.width = width
        self.length = length
        self.object_id = object_id
        self.sec = sec
        self.nsec = nsec
        self.type = type
        self.tracked_object = tracked_object

    def give_arrow_marker(self):
        ratio = 3
        marker = Marker()
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        if self.type == 1:
            ratio = 3
            marker.color.a = 1.0
            marker.color.r = 1.0
            marker.color.g = 1.0
            marker.color.b = 1.0
        elif self.type == 2:
            ratio = 1
        elif self.type == 3:
            ratio = 1
        elif self.type == 4:
            ratio = 1

        start_x = self.center_x
        start_y = self.center_y
        start_z = float(self.center_z)

        yaw = self.heading

        end_x = start_x + np.cos(yaw) * ratio
        end_y = start_y + np.sin(yaw) * ratio
        end_z = float(start_z)

        marker.header.frame_id = "map"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.id = int(self.object_id)
        marker.ns = "marker_arrow"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)

        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.4

        p1 = Point()
        p2 = Point()
        p1.x = start_x
        p1.y = start_y
        p1.z = float(start_z)
        p2.x = end_x
        p2.y = end_y
        p2.z = float(end_z)
        marker.points.append(p1)
        marker.points.append(p2)
        return marker

    def give_cube_marker(self):
        marker = Marker()
        vehicle = (255.0, 255.0, 255.0)
        pedestrian = (255.0, 255.0, 255.0)
        cyclist = (255.0, 255.0, 255.0)
        sign = (255.0, 255.0, 255.0)
        marker.header.frame_id = "map"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.type = Marker.CUBE
        marker.action = Marker.ADD
        marker.id = int(self.object_id)
        marker.ns = "marker_cube"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        marker.pose.position.x = self.center_x
        marker.pose.position.y = self.center_y
        marker.pose.position.z = float(self.center_z)

        q = Quaternion.from_euler(0, 0, self.heading, degrees=False)

        marker.pose.orientation.x = q.x
        marker.pose.orientation.y = q.y
        marker.pose.orientation.z = q.z
        marker.pose.orientation.w = q.w

        marker.scale.x = self.length
        marker.scale.y = self.width
        marker.scale.z = self.height
        marker.color.a = 0.5
        if self.type == 1:
            marker.color.b = vehicle[0]
            marker.color.g = vehicle[1]
            marker.color.r = vehicle[2]

        elif self.type == 2:
            marker.color.b = pedestrian[0]
            marker.color.g = pedestrian[1]
            marker.color.r = pedestrian[2]

        elif self.type == 3:
            marker.color.b = sign[0]
            marker.color.g = sign[1]
            marker.color.r = sign[2]

        elif self.type == 4:
            marker.color.b = cyclist[0]
            marker.color.g = cyclist[1]
            marker.color.r = cyclist[2]

        return marker

    def give_id_marker(self):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "map"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.id = int(self.object_id)
        marker.ns = "marker_text"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = self.center_x
        marker.pose.position.y = self.center_y
        marker.pose.position.z = float(self.center_z + 1)
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        marker.text = str(self.object_id)
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker

    def giveMarkerPoints(self):
        marker = Marker()
        marker.header.frame_id = "map"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.type = Marker.POINTS
        marker.action = Marker.ADD
        marker.id = int(self.object_id)
        marker.ns = "marker_center_points"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        marker.scale.x = 0.7
        marker.scale.y = 0.7
        marker.scale.z = 0.7
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0

        point = Point()
        point.x = self.center_x
        point.y = self.center_y
        point.z = float(self.center_z)
        marker.points.append(point)

        return marker

    def give_marker_class_name(self):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "map"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = self.sec
        timestamp.nanosec = self.nsec
        marker.header.stamp = timestamp
        marker.id = int(self.object_id)
        marker.ns = "marker_text_class_name"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = self.center_x
        marker.pose.position.y = self.center_y
        marker.pose.position.z = self.center_z + 2.5
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        class_name = ''
        if self.type == 1:
            class_name = 'vehicle'
        if self.type == 2:
            class_name = 'Pedestrian'
        if self.type == 4:
            class_name = 'Cyclist'
        marker.text = class_name
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker
