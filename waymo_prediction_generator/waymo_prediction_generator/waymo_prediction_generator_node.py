import rclpy
from rclpy.node import Node
from helper import Helper
from helper import Visualizer

# Alternative for tf py
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import numpy as np
from autoware_auto_msgs.msg import TrackedObject
from autoware_auto_msgs.msg import TrackedObjects
from waymo_open_dataset.protos import metrics_pb2
from waymo_open_dataset import label_pb2
import sys
import os
import signal


class DetectionPublisherNode(Node):
    def __init__(self):
        super().__init__('waymo_prediction_generator_node')

        path_gt_detection_vehicle = sys.argv[1]
        path_gt_detection_pedestrian = sys.argv[2]
        path_gt_detection_cyclist = sys.argv[3]
        path_tf_record_folder = sys.argv[4]
        segment_name = sys.argv[5]
        path_output_file = sys.argv[6]
        path_tf_record = path_tf_record_folder + '/' + segment_name

        self.helper = Helper(path_tf_record,
                             path_gt_detection_vehicle,
                             path_gt_detection_pedestrian,
                             path_gt_detection_cyclist)
        self.helper.read_baseline_detection_files()
        self.helper.read_tfrecord()

        # Subscribe tracked objects:
        self.subscription = self.create_subscription(TrackedObjects, '/tracked_objects', self.CallbackTrackedObjects,
                                                     10)
        # Publish MarkerArr:
        #self.pub_markerArr = self.create_publisher(MarkerArray, "/markerArray_tracked_object_ids", 0)
        # Proto objects all:
        self.proto_objects_calculated = metrics_pb2.Objects()
        self.output_path = str(path_output_file)
        self.f = open(self.output_path, 'ab+')

    def CallbackTrackedObjects(self, tracked_objects):

        frame_id, timestamp_waymo, context_name, total_frame_count = self.helper.get_dataset_timestamp(
            tracked_objects.header.stamp.sec,
            tracked_objects.header.stamp.nanosec)
        print("Total frame count: ", total_frame_count)
        print("FRAME INFO:")
        print("Tracked objects are received. Frame id:", frame_id)
        print("Tracked object count", len(tracked_objects.objects))
        print("Total ground truth object count in this frame: ", self.helper.get_gt_object_count(frame_id), "\n\n")

        for tracked_object in tracked_objects.objects:

            center_x, center_y, center_z, heading, height, length, width, object_id, type \
                = self.helper.give_tracked_object_info(tracked_object)

            #closest_obj_baseline, closest_obj_gt = self.helper.get_closest_objects(frame_id, tracked_object)

            # if ped or cyclist
            '''if type == 1 or type == 2 or type == 4:
                # Print tracked object info:
                if type==1:
                    print("VEHICLE OBJECT:")
                if type==2:
                    print("PED OBJECT:")
                if type==4:
                    print("CYCLIST OBJECT:")

                print("Tracked object id:", object_id, "| x:", center_x, "| y:", center_y, "| z:", center_z,
                      "| heading:", heading, "| length:", length, "| width:", width, "| height:", height)

                print("Closest ground truth object:| x:", closest_obj_gt.box.center_x, "| y:",
                      closest_obj_gt.box.center_y, "| z:",
                      closest_obj_gt.box.center_z,
                      "| heading:", closest_obj_gt.box.heading, "| length:", closest_obj_gt.box.length,
                      "| width:", closest_obj_gt.box.width, "| height:", closest_obj_gt.box.height, "| type:",closest_obj_gt.type)

                print("Closest baseline object:| x:", closest_obj_baseline.object.box.center_x, "| y:",
                      closest_obj_baseline.object.box.center_y, "| z:",
                      closest_obj_baseline.object.box.center_z,
                      "| heading:", closest_obj_baseline.object.box.heading, "| length:", closest_obj_baseline.object.box.length,
                      "| width:", closest_obj_baseline.object.box.width, "| height:", closest_obj_baseline.object.box.height,
                      "| type:",closest_obj_baseline.object.type)

                print("\n")'''


            # Generate predicted object:
            object_calculated = metrics_pb2.Object()
            object_calculated.context_name = context_name
            object_calculated.frame_timestamp_micros = timestamp_waymo
            box = label_pb2.Label.Box()
            box.center_x = center_x
            box.center_y = center_y
            box.center_z = center_z
            box.length = length
            box.width = width
            box.height = height
            box.heading = heading
            object_calculated.object.box.CopyFrom(box)
            object_calculated.score = 1.0
            object_calculated.object.id = object_id
            object_calculated.object.type = type
            self.proto_objects_calculated.objects.append(object_calculated)

        if frame_id == total_frame_count - 2:
            self.f.write(self.proto_objects_calculated.SerializeToString())
            self.f.close()
            print("Prediction file is generated:", self.output_path)
            os.kill(os.getpid(), signal.SIGTERM)


def main(args=None):
    rclpy.init(args=args)
    node_obj = DetectionPublisherNode()
    rclpy.spin(node_obj)
    node_obj.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
