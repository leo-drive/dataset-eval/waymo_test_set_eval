Populate a folder with .tfrecord files, then give the folder path as a first command line argument.
```bash
python3 tfrecord_to_ros2bag_converter.py /home/goktug/Desktop/Waymo_Valid_Dataset/valid0/validation_validation_0000 /home/goktug/ros2bags
```
### Requirements
```bash
ROS2 Rolling
squaternion 0.3.2
```
To build and install ROS2 Rolling, follow those instructions:
https://github.com/leo-drive/proto_to_rosbag2

Install squaternion: pip3 install squaternion

