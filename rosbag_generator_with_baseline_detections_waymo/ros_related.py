from os import listdir
from os.path import isfile, join
import numpy as np
import os
import math
import itertools
import codecs
# tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset

from sensor_msgs.msg import PointCloud2, PointField, Image, CameraInfo
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import TransformStamped
from tf2_msgs.msg import TFMessage
import geometry_msgs.msg

# Alternative for tf py
from squaternion import Quaternion

import rclpy
from rclpy.duration import Duration
import builtin_interfaces.msg
from std_msgs.msg import Header

import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

# for ground truth objects:
from autoware_auto_msgs.msg import DetectedObjects
from autoware_auto_msgs.msg import DetectedObject
from autoware_auto_msgs.msg import DetectedObjectKinematics
from autoware_auto_msgs.msg import ObjectClassification
from autoware_auto_msgs.msg import Shape
from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32
from geometry_msgs.msg import PoseWithCovariance
from geometry_msgs.msg import TwistWithCovariance

from nav_msgs.msg import Odometry

from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker

from polygon_point_generator import PolygonPointGenerator

from KalmanFilter import KF
from waymo_stuff import WaymoStuff
from polygon_point_generator import PolygonPointGenerator

from autoware_auto_msgs.msg import BoundingBoxArray
from autoware_auto_msgs.msg import BoundingBox


class ROSRelated(object):
    def __init__(self):
        self.list_vehicle_pos_x = []
        self.list_vehicle_pos_y = []
        self.list_vehicle_pos_z = []
        self.list_vehicle_roll_rad = []
        self.list_vehicle_pitch_rad = []
        self.list_vehicle_yaw_rad = []

        self.list_vehicle_vel_x = []
        self.list_vehicle_vel_y = []
        self.list_vehicle_vel_z = []
        self.list_vehicle_vel_x_angular = []
        self.list_vehicle_vel_y_angular = []
        self.list_vehicle_vel_z_angular = []

    def update_list(self, list_vehicle_pos_x,
                    list_vehicle_pos_y,
                    list_vehicle_pos_z,
                    list_vehicle_roll_rad,
                    list_vehicle_pitch_rad,
                    list_vehicle_yaw_rad,
                    list_vehicle_vel_x,
                    list_vehicle_vel_y,
                    list_vehicle_vel_z,
                    list_vehicle_vel_x_angular,
                    list_vehicle_vel_y_angular,
                    list_vehicle_vel_z_angular):
        self.list_vehicle_pos_x = list_vehicle_pos_x
        self.list_vehicle_pos_y = list_vehicle_pos_y
        self.list_vehicle_pos_z = list_vehicle_pos_z
        self.list_vehicle_roll_rad = list_vehicle_roll_rad
        self.list_vehicle_pitch_rad = list_vehicle_pitch_rad
        self.list_vehicle_yaw_rad = list_vehicle_yaw_rad

        self.list_vehicle_vel_x = list_vehicle_vel_x
        self.list_vehicle_vel_y = list_vehicle_vel_y
        self.list_vehicle_vel_z = list_vehicle_vel_z
        self.list_vehicle_vel_x_angular = list_vehicle_vel_x_angular
        self.list_vehicle_vel_y_angular = list_vehicle_vel_y_angular
        self.list_vehicle_vel_z_angular = list_vehicle_vel_z_angular

    def giveIdLabelText(self, frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.id = ID + frame_id
        marker.ns = "marker_text"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = laser_label.box.center_x
        marker.pose.position.y = laser_label.box.center_y
        marker.pose.position.z = laser_label.box.center_z + 1
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        marker.text = str(ID)
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker

    def giveCubeLabel(self, frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id, type, ns, gt):
        marker = Marker()
        if type == 'vehicle':
            marker.ns = ns
            marker.color.a = 0.8
            marker.color.b = 1.0
            marker.color.g = 0.0
            marker.color.r = 0.0
        if type == 'pedestrian':
            marker.ns = ns
            marker.color.a = 0.8
            marker.color.b = 0.0
            marker.color.g = 1.0
            marker.color.r = 0.0
        if type == 'cyclist':
            marker.ns = ns
            marker.color.a = 0.8
            marker.color.b = 0.0
            marker.color.g = 0.0
            marker.color.r = 1.0

        if gt:
            marker.color.a = 0.8
            marker.color.b = 1.0
            marker.color.g = 1.0
            marker.color.r = 1.0


        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.type = Marker.CUBE
        marker.action = Marker.ADD
        marker.id = ID + frame_id
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        marker.pose.position.x = laser_label.box.center_x
        marker.pose.position.y = laser_label.box.center_y
        marker.pose.position.z = laser_label.box.center_z
        q = Quaternion.from_euler(0, 0, laser_label.box.heading, degrees=False)
        q = q.normalize
        marker.pose.orientation.x = q.x  # quaternion[1]
        marker.pose.orientation.y = q.y  # quaternion[2]
        marker.pose.orientation.z = q.z  # quaternion[3]
        marker.pose.orientation.w = q.w  # quaternion[0]
        marker.scale.x = laser_label.box.length
        marker.scale.y = laser_label.box.width
        marker.scale.z = laser_label.box.height

        return marker

    def giveArrowMarker(self, frame_stamp_secs, frame_stamp_nsecs, laser_label, ID, frame_id, type, ns, gt):

        ratio = 3
        marker = Marker()

        if type == 'vehicle':
            marker.ns = ns
        if type == 'pedestrian':
            marker.ns = ns
        if type == 'cyclist':
            marker.ns = ns

        marker.color.a = 1.0
        marker.color.r = 0.5
        marker.color.g = 1.0
        marker.color.b = 0.5
        if laser_label.type == 1:
            ratio = 3
            marker.color.a = 1.0
            marker.color.r = 0.0
            marker.color.g = 0.0
            marker.color.b = 1.0
        elif laser_label.type == 2:
            ratio = 1
        elif laser_label.type == 3:
            ratio = 1
        elif laser_label.type == 4:
            ratio = 1

        start_x = laser_label.box.center_x
        start_y = laser_label.box.center_y
        start_z = laser_label.box.center_z

        yaw = laser_label.box.heading

        end_x = start_x + np.cos(yaw) * ratio
        end_y = start_y + np.sin(yaw) * ratio
        end_z = start_z

        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.id = ID + frame_id

        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)

        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.4

        p1 = geometry_msgs.msg.Point()
        p2 = geometry_msgs.msg.Point()
        p1.x = start_x
        p1.y = start_y
        p1.z = start_z
        p2.x = end_x
        p2.y = end_y
        p2.z = end_z
        marker.points.append(p1)
        marker.points.append(p2)
        return marker

    def giveVehicleVelo(self, frame_stamp_secs, frame_stamp_nsecs, vx_linear,
                        vy_linear, vz_linear, wx_angular, wy_angular, wz_angular):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.id = 2147483646
        marker.ns = "marker_text_velo"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = 0.0
        marker.pose.position.y = -2.0
        marker.pose.position.z = 1.2
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 1.0
        marker.pose.orientation.w = 1.0
        text = 'vx:' + str(round(float(vx_linear), 3)) + 'm/s\nvy:' + str(round(float(vy_linear), 3)) + \
               'm/s\nvz:' + str(round(float(vz_linear), 3)) + 'm/s\nwx:' + str(round(float(wx_angular * 57.29), 3)) + \
               'degree/s\nwy:' + str(round(float(wy_angular * 57.29), 3)) + 'degree/s\nwz:' + str(
            round(float(wz_angular * 57.29), 3)) + 'degree/s'

        marker.text = str(text)
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker

    def give_odom_msg(self, header, child_frame_id,
                      x, y, z, qx, qy, qz, qw, vx, vy, vz, wx, wy, wz, give_pose_cov,
                      give_twist_cov):
        odometry_msg = Odometry()  # Members: 1) PoseWithCovariance 2) TwistWithCovariance
        odometry_msg.header = header
        odometry_msg.child_frame_id = child_frame_id

        pose_and_cov = PoseWithCovariance()
        # Pose:
        pose_and_cov.pose.position.x = x
        pose_and_cov.pose.position.y = y
        pose_and_cov.pose.position.z = z
        pose_and_cov.pose.orientation.x = qx
        pose_and_cov.pose.orientation.y = qy
        pose_and_cov.pose.orientation.z = qz
        pose_and_cov.pose.orientation.w = qw
        # Pose Covariance:
        if give_pose_cov:
            data = np.array([self.list_vehicle_pos_x,
                             self.list_vehicle_pos_y,
                             self.list_vehicle_pos_z,
                             self.list_vehicle_roll_rad,
                             self.list_vehicle_pitch_rad,
                             self.list_vehicle_yaw_rad])
            covMatrix_vehicle_pose = np.cov(data, bias=True)
            pose_and_cov.covariance = covMatrix_vehicle_pose.reshape((1, 36)).ravel()
        # Twist:
        twist_with_cov = TwistWithCovariance()
        twist_with_cov.twist.linear.x = float(vx)
        twist_with_cov.twist.linear.y = float(vy)
        twist_with_cov.twist.linear.z = float(vz)
        twist_with_cov.twist.angular.x = float(wx)
        twist_with_cov.twist.angular.y = float(wy)
        twist_with_cov.twist.angular.z = float(wz)
        # Twist Covariance:
        if give_twist_cov:
            data = np.array([self.list_vehicle_vel_x,
                             self.list_vehicle_vel_y,
                             self.list_vehicle_vel_z,
                             self.list_vehicle_vel_x_angular,
                             self.list_vehicle_vel_y_angular,
                             self.list_vehicle_vel_z_angular]).reshape((6, len(self.list_vehicle_vel_x)))
            covMatrix_vehicle_twist = np.cov(data, bias=True)
            twist_with_cov.covariance = covMatrix_vehicle_twist.reshape((1, 36)).ravel()

        odometry_msg.pose = pose_and_cov
        odometry_msg.twist = twist_with_cov

        return odometry_msg

    def give_vehicle_arrow_marker(self, frame_stamp_secs, frame_stamp_nsecs, dx, dy, dz, heading, frame_id):

        marker = Marker()
        marker.color.a = 1.0
        marker.color.r = 0.5
        marker.color.g = 1.0
        marker.color.b = 0.5

        ratio = 6
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 1.0

        start_x = dx
        start_y = dy
        start_z = dz

        yaw = heading

        end_x = start_x + np.cos(yaw) * ratio
        end_y = start_y + np.sin(yaw) * ratio
        end_z = start_z

        marker.header.frame_id = frame_id
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.type = Marker.ARROW
        marker.action = Marker.ADD
        marker.id = 0
        marker.ns = "marker_arrow"
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0,
                                                          nanosec=100000000)

        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.4

        p1 = geometry_msgs.msg.Point()
        p2 = geometry_msgs.msg.Point()
        p1.x = start_x
        p1.y = start_y
        p1.z = start_z
        p2.x = end_x
        p2.y = end_y
        p2.z = end_z
        marker.points.append(p1)
        marker.points.append(p2)
        return marker

    def give_marker_class_name(self, frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id,
                               object_type):
        marker = Marker()
        marker.action = Marker.ADD
        marker.header.frame_id = "vehicle"
        timestamp = builtin_interfaces.msg.Time()
        timestamp.sec = frame_stamp_secs
        timestamp.nanosec = frame_stamp_nsecs
        marker.header.stamp = timestamp
        marker.id = ID + frame_id
        marker.ns = "marker_text_class_name"
        marker.type = Marker.TEXT_VIEW_FACING
        marker.pose.position.x = laser_label.box.center_x
        marker.pose.position.y = laser_label.box.center_y
        marker.pose.position.z = laser_label.box.center_z 
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0
        class_name = ''
        if object_type == 1:
            class_name = 'vehicle'
        if object_type == 6:
            class_name = 'Pedestrian'
        if object_type == 5:
            class_name = 'Cyclist'
        marker.text = class_name
        marker.lifetime = builtin_interfaces.msg.Duration(sec=0, nanosec=100000000)
        return marker

    def give_detect_objects_msg(self, list_objects,
                                frame_stamp_secs,
                                frame_stamp_nsecs,
                                detected_objects_msg,
                                lidar_label_markerArr,
                                bboxArr):
        for id_, object in enumerate(list_objects):
            detected_object = DetectedObject()
            object_classification = ObjectClassification()
            kinematics = DetectedObjectKinematics()
            shape = Shape()
            pose_and_cov = PoseWithCovariance()
            twist_with_cov = TwistWithCovariance()

            # Classification:
            # Mapping Waymo classes to Autoware.Auto classes:
            object_type = -1
            if object.object.type == 1:  # Vehicle
                object_type = 1
            elif object.object.type == 2:  # Pedestrian
                object_type = 6
            elif object.object.type == 4:  # Cyclist
                object_type = 5
            object_classification.classification = object_type
            object_classification.probability = 1.0
            detected_object.classification.append(object_classification)
            # Existence Probability:
            detected_object.existence_probability = 1.0
            # Kinematics:
            # Pose:
            pose_and_cov.pose.position.x = object.object.box.center_x
            pose_and_cov.pose.position.y = object.object.box.center_y
            pose_and_cov.pose.position.z = object.object.box.center_z
            q = Quaternion.from_euler(0, 0, object.object.box.heading, degrees=False)
            pose_and_cov.pose.orientation.x = q.x
            pose_and_cov.pose.orientation.y = q.y
            pose_and_cov.pose.orientation.z = q.z
            pose_and_cov.pose.orientation.w = q.w

            kinematics.pose = pose_and_cov
            # Twist:
            bool_twist = False
            if bool_twist:
                twist_with_cov.twist.linear.x = object.object.metadata.speed_x
                twist_with_cov.twist.linear.y = object.object.metadata.speed_y
                twist_with_cov.twist.linear.z = 0.0

                kinematics.twist = twist_with_cov

            kinematics.has_pose = True
            kinematics.has_pose_covariance = False
            kinematics.has_twist = bool_twist
            kinematics.has_twist_covariance = False
            detected_object.kinematics = kinematics

            # Shape:
            polygon_point_generator = PolygonPointGenerator(object.object.box.center_x,
                                                            object.object.box.center_y,
                                                            object.object.box.center_z,
                                                            object.object.box.length,
                                                            object.object.box.width,
                                                            object.object.box.heading,
                                                            id_,
                                                            "vehicle",
                                                            frame_stamp_secs,
                                                            frame_stamp_nsecs,
                                                            object_type)
            marker_polygon_points, list_polygon_points = polygon_point_generator.giveMarkerPolygonPointsANDPolygon()
            lidar_label_markerArr.markers.append(marker_polygon_points)

            for point in list_polygon_points:
                shape.polygon.points.append(point)
            shape.height = object.object.box.height
            detected_object.shape = shape

            detected_objects_msg.objects.append(detected_object)

            bbox = BoundingBox()
            bbox.centroid.x = object.object.box.center_x
            bbox.centroid.y = object.object.box.center_y
            bbox.centroid.z = object.object.box.center_z
            bbox.orientation.x = q.x
            bbox.orientation.y = q.y
            bbox.orientation.z = q.z
            bbox.orientation.w = q.w
            bbox.size.x = object.object.box.length
            bbox.size.y = object.object.box.width
            bbox.size.z = object.object.box.height
            bbox.heading = object.object.box.heading

            object_type = -1
            if object.object.type == 1:  # Vehicle
                object_type = 1
            elif object.object.type == 2:  # Pedestrian
                object_type = 6
            elif object.object.type == 4:  # Cyclist
                object_type = 5
            bbox.vehicle_label = object_type
            bboxArr.boxes.append(bbox)

        return detected_objects_msg, lidar_label_markerArr, bboxArr
