### Requirements
```bash
ROS2 Rolling
squaternion 0.3.2
waymo-open-dataset-tf-2-1-0
```
To build and install ROS2 Rolling, follow those instructions:
https://github.com/leo-drive/proto_to_rosbag2

### Install Python Dependencies
```bash
pip3 install squaternion
pip3 install waymo-open-dataset-tf-2-1-0==1.2.0
```
### Install Waymo Open Dataset tfrecords
```bash
https://console.cloud.google.com/storage/browser/waymo_open_dataset_v_1_2_0_detection_output
```
#### Install Waymo Open Dataset Baseline Detections
```bash
https://console.cloud.google.com/storage/browser/waymo_open_dataset_v_1_2_0_detection_output
```
**Note that:** You have to use appropriate autoware_auto_msgs package that we provide.
### How to use rosbag generator using Waymo baseline detections
```bash
source autoware_auto_msgs/install/setup.bash
source ros2_rolling/install/setup.bash
python3 rosbag_generator.py 
    /home/goktug/projects/waymo_test_set_eval/dataset_stuff/tfrecords 
    /home/goktug/projects/waymo_test_set_eval/dataset_stuff/rosbags
    /home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_vehicle_detection_test.bin 
    /home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_pedestrian_detection_test.bin 
    /home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_cyclist_detection_test.bin
```

### How to use Waymo prediction generator tool
Use ROS2 Foxy with Waymo prediction generator tool.

The main purpose of this tool is to generate a binary prediction file. You can use validation or test set in order to generate a prediction file.

If you would like to use validation set, you can upload this binary file to Waymo Open Dataset website to get evaluation results, or carry out the offline evaluation.

If you would like to use test set, you have to  upload this binary file to Waymo Open Dataset website.

The version of autoware_auto_msgs package in the Autoware.Auto is the same as autoware_auto_msgs we provide.

Rosbag folder should consist of '.db3' files.
### Parameters of run_eval.py:
```bash
node_path = '/home/goktug/projects/waymo_test_set_eval/waymo_prediction_generator/waymo_prediction_generator/waymo_prediction_generator_node.py'
path_baseline_det_vehicle = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_vehicle_detection_test.bin'
path_baseline_det_ped = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_vehicle_detection_test.bin'
path_baseline_det_cyc = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/gt_detections/detection_3d_cyclist_detection_test.bin'
path_tfrecords_folder = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/tfrecords'
path_rosbag_folder = '/home/goktug/projects/waymo_test_set_eval/dataset_stuff/rosbags'
path_output_file = '/home/goktug/Desktop/pred_autoware.bin'
path_tf_file = 'segment-15865907199900332614_760_000_780_000_with_camera_labels.tfrecord'
```
#### Include those code snippets into your tracker
```bash
#include<csignal>
#include <iostream>
#include <unistd.h>

// They have to be started with the tracker node!
auto t1 = high_resolution_clock::now();
auto t2 = high_resolution_clock::now();

timer_ = this->create_wall_timer(
        std::chrono::milliseconds(1),
        std::bind(&MultiObjectTrackerNode::timerCallback, this));

void MultiObjectTrackerNode::timerCallback()
{
    t1 = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(t1-t2);
    //std::cout << "millisecond:" << duration.count() << std::endl;

    if (duration.count() > 15000)
    {
        auto pid = getpid();
        kill(pid, SIGTERM);
    }
}
// This must be in the tracking callback!
t2 = high_resolution_clock::now();
```
```bash
source Autoware.Auto/install/setup.bash
python3 run_eval.py
```
### How to carry out the offline evaluation using the Waymo evaluation script with validation set
You have to install and build Waymo Open dataset from source.

Recommended TensorFlow version is 2.1.0 to build Waymo Open Dataset from source.
```bash
https://github.com/waymo-research/waymo-open-dataset
https://github.com/waymo-research/waymo-open-dataset/blob/master/tutorial/tutorial.ipynb
```
Before building the Waymo Open Dataset from source, replace /waymo_open_dataset/metrics/tools/compute_tracking_metrics_main.cc with it which we provide.

You can use instructions in the tutorial to build.

Download ground truth file 'gt.bin' in the validation set.

Generate prediction_file.bin using prediction generator tool.

To evaluate:
```bash
./compute_tracking_metrics_main my_prediction_file.bin gt.bin
```