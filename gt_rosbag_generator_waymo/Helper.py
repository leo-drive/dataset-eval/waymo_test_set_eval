from os import listdir
from os.path import isfile, join
import numpy as np
import os
import tensorflow.compat.v1 as tf
import math
import itertools
import codecs
# tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset

from sensor_msgs.msg import PointCloud2, PointField, Image, CameraInfo
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import TransformStamped
from tf2_msgs.msg import TFMessage
import geometry_msgs.msg

# Alternative for tf py
from squaternion import Quaternion

import rclpy
from rclpy.duration import Duration
import builtin_interfaces.msg
from std_msgs.msg import Header

import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

# for ground truth objects:
from autoware_auto_msgs.msg import DetectedObjects
from autoware_auto_msgs.msg import DetectedObject
from autoware_auto_msgs.msg import DetectedObjectKinematics
from autoware_auto_msgs.msg import ObjectClassification
from autoware_auto_msgs.msg import Shape
from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32
from geometry_msgs.msg import PoseWithCovariance
from geometry_msgs.msg import TwistWithCovariance
from waymo_open_dataset.protos import metrics_pb2
from nav_msgs.msg import Odometry
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker
from polygon_point_generator import PolygonPointGenerator
from KalmanFilter import KF
from waymo_stuff import WaymoStuff
from ros_related import ROSRelated
from autoware_auto_msgs.msg import BoundingBoxArray
from autoware_auto_msgs.msg import BoundingBox

class Helper(object):
    def __init__(self, path_tfrecord):
        self.path_tfrecord = path_tfrecord

        self.segment_name = self.path_tfrecord.split('/')[-1].split('-')[-1].split('.')[0].split('_with')[0]

        self.list_types = [1, 2, 4]

        self.waymoStuff = WaymoStuff()
        self.list_frame_by_frame_lidar_labels = []
        self.list_frame_by_frame_detected_objects = []
        self.list_frame_by_frame_trans = []
        self.list_frame_by_frame_odometry = []
        self.list_frame_by_frame_bbox_array = []
        self.list_timestamp = []

        self.trans_t_zero_inverse = np.array([0])

        self.kalman_x = KF(0, 0, accel_variance=0.9)
        self.kalman_y = KF(0, 0, accel_variance=0.9)
        self.kalman_z = KF(0, 0, accel_variance=0.9)
        self.kalman_roll = KF(0, 0, accel_variance=0.9)
        self.kalman_pitch = KF(0, 0, accel_variance=0.9)
        self.kalman_heading = KF(0, 0, accel_variance=0.9)
        self.meas_variance = 0.5

        self.list_vehicle_pos_x = []
        self.list_vehicle_pos_y = []
        self.list_vehicle_pos_z = []
        self.list_vehicle_roll_rad = []
        self.list_vehicle_pitch_rad = []
        self.list_vehicle_yaw_rad = []
        self.list_vehicle_vel_x = []
        self.list_vehicle_vel_y = []
        self.list_vehicle_vel_z = []
        self.list_vehicle_vel_x_angular = []
        self.list_vehicle_vel_y_angular = []
        self.list_vehicle_vel_z_angular = []

        self.ros_related = ROSRelated()


    def readDataset(self):
        dataset = tf.data.TFRecordDataset(self.path_tfrecord, compression_type='')
        list_frame_ids = []
        print("\n\n\n\n\nSEGMENT NAME:", self.segment_name)

        for frame_id, data in enumerate(dataset):
            lidar_label_markerArr = MarkerArray()
            transforms = TFMessage()
            list_frame_ids.append(frame_id)
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            print("Frame id: ", frame_id)
            frame_waymo_time = frame.timestamp_micros
            frame_stamp_secs = int(frame.timestamp_micros / 1000000)
            frame_stamp_nsecs = int((frame.timestamp_micros / 1000000.0 - frame_stamp_secs) * 1000000000)
            timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)

            # Collect Ground Truth Objects to Visualize:
            ############################################################################################################

            ############################################################################################################
            #
            #
            #
            #
            # Collect dynamic transforms ##############################################################################:
            transform = TransformStamped()
            transform.header.frame_id = "map"
            timestamp = builtin_interfaces.msg.Time()
            timestamp.sec = frame_stamp_secs
            timestamp.nanosec = frame_stamp_nsecs
            transform.header.stamp = timestamp

            mat_trans_frame = np.array([(frame.pose.transform[0], frame.pose.transform[1], frame.pose.transform[2],
                                         frame.pose.transform[3]),
                                        (frame.pose.transform[4], frame.pose.transform[5], frame.pose.transform[6],
                                         frame.pose.transform[7]),
                                        (frame.pose.transform[8], frame.pose.transform[9], frame.pose.transform[10],
                                         frame.pose.transform[11]),
                                        (0, 0, 0, 1)])
            # Define T inverse of odom:
            if frame_id == 0:
                self.trans_t_zero_inverse = np.linalg.inv(mat_trans_frame)

            mat_trans_frame = np.dot(self.trans_t_zero_inverse, mat_trans_frame)
            rotation_matrix = mat_trans_frame[0:3, 0:3]

            r, p, y = self.waymoStuff.rotationMatrixToEulerAngles(rotation_matrix)

            self.list_vehicle_roll_rad.append(r)
            self.list_vehicle_pitch_rad.append(p)
            self.list_vehicle_yaw_rad.append(y)

            q = Quaternion.from_euler(r, p, y, degrees=False)
            coeff = 1
            if (q.z < 0):
                coeff = -1

            transform.transform.rotation.x = q.x * coeff
            transform.transform.rotation.y = q.y * coeff
            transform.transform.rotation.z = q.z * coeff
            transform.transform.rotation.w = q.w * coeff
            transform.transform.translation.x = mat_trans_frame[0][3]
            transform.transform.translation.y = mat_trans_frame[1][3]
            transform.transform.translation.z = mat_trans_frame[2][3]
            transform.child_frame_id = "vehicle"
            transforms.transforms.append(transform)
            # Collect dynamic transforms ##############################################################################:
            #
            #
            #
            #
            #
            # Kalman Stuff ############################################################################################:
            # All of the states are in the map frame!
            # Vx Vy Vz linear:
            self.kalman_x.update(mat_trans_frame[0][3], self.meas_variance)
            self.kalman_y.update(mat_trans_frame[1][3], self.meas_variance)
            self.kalman_z.update(mat_trans_frame[2][3], self.meas_variance)
            (_, vel_x) = self.kalman_x.predict(0.1)
            (_, vel_y) = self.kalman_y.predict(0.1)
            (_, vel_z) = self.kalman_z.predict(0.1)
            self.list_vehicle_pos_x.append(mat_trans_frame[0][3])
            self.list_vehicle_pos_y.append(mat_trans_frame[1][3])
            self.list_vehicle_pos_z.append(mat_trans_frame[2][3])
            self.list_vehicle_vel_x.append(vel_x)
            self.list_vehicle_vel_y.append(vel_y)
            self.list_vehicle_vel_z.append(vel_z)
            # Vx_angular Vy_angular Vz_angular:
            self.kalman_roll.update(r, self.meas_variance)
            self.kalman_pitch.update(p, self.meas_variance)
            self.kalman_heading.update(y, self.meas_variance)
            (_, vel_x_angular) = self.kalman_roll.predict(0.1)
            (_, vel_y_angular) = self.kalman_pitch.predict(0.1)
            (_, vel_z_angular) = self.kalman_heading.predict(0.1)
            self.list_vehicle_vel_x_angular.append(vel_x_angular)
            self.list_vehicle_vel_y_angular.append(vel_y_angular)
            self.list_vehicle_vel_z_angular.append(vel_z_angular)
            self.ros_related.update_list(self.list_vehicle_pos_x,
                                         self.list_vehicle_pos_y,
                                         self.list_vehicle_pos_z,
                                         self.list_vehicle_roll_rad,
                                         self.list_vehicle_pitch_rad,
                                         self.list_vehicle_yaw_rad,
                                         self.list_vehicle_vel_x,
                                         self.list_vehicle_vel_y,
                                         self.list_vehicle_vel_z,
                                         self.list_vehicle_vel_x_angular,
                                         self.list_vehicle_vel_y_angular,
                                         self.list_vehicle_vel_z_angular)
            marker_text_velocities = self.ros_related.giveVehicleVelo(frame_stamp_secs, frame_stamp_nsecs,
                                                                      vel_x,
                                                                      vel_y, vel_z,
                                                                      vel_x_angular,
                                                                      vel_y_angular,
                                                                      vel_z_angular)
            lidar_label_markerArr.markers.append(marker_text_velocities)
            marker_vehicle_heading_arrow = self.ros_related.give_vehicle_arrow_marker(frame_stamp_secs,
                                                                                      frame_stamp_nsecs,
                                                                                      transform.transform.translation.x,
                                                                                      transform.transform.translation.y,
                                                                                      transform.transform.translation.z,
                                                                                      y, "map")
            lidar_label_markerArr.markers.append(marker_vehicle_heading_arrow)

            print("Position in map frame dx dy dz: ", mat_trans_frame[0][3],
                  mat_trans_frame[1][3],
                  mat_trans_frame[2][3])
            '''print("Velocity in map frame vx vy vz: ", vel_x, vel_y, vel_z)
            print("dt: 0.1s")'''
            ############################################################################################################
            #
            #
            #
            #
            #
            # Collect odometry msgs ####################################################################################:
            odometry_msg = self.ros_related.give_odom_msg(transform.header, 'vehicle',
                                                          transform.transform.translation.x,
                                                          transform.transform.translation.y,
                                                          transform.transform.translation.z,
                                                          transform.transform.rotation.x,
                                                          transform.transform.rotation.y,
                                                          transform.transform.rotation.z,
                                                          transform.transform.rotation.w,
                                                          vel_x, vel_y, vel_z,
                                                          vel_x_angular, vel_y_angular, vel_z_angular,
                                                          False, False)

            self.list_frame_by_frame_trans.append([transforms, '/tf', timestamp_bag_nano])
            self.list_frame_by_frame_odometry.append([odometry_msg, '/odometry', timestamp_bag_nano])
            # END Odom #################################################################################################
            #
            #
            #
            #
            #
            # START: ground truth objects as a Autoware.Auto detected objects msgs #####################################
            timestamp = builtin_interfaces.msg.Time()
            timestamp.sec = frame_stamp_secs
            timestamp.nanosec = frame_stamp_nsecs

            bboxArr = BoundingBoxArray()
            bboxArr.header.frame_id = "vehicle"
            bboxArr.header.stamp = timestamp

            # Iterate over ground truths in a single frame:
            for index, laser_label in enumerate(frame.laser_labels):

                bbox = BoundingBox()

                ID = codecs.encode(laser_label.id[len(laser_label.id) - 8:len(laser_label.id)])
                ID = int.from_bytes(ID, byteorder='big', signed=False)
                ID = int(str(ID)[:5])

                # Mapping Waymo classes to Autoware.Auto classes:
                object_type = -1
                type_ = ""
                if laser_label.type == 1:  # Vehicle
                    object_type = 1
                    type_ = "vehicle"
                elif laser_label.type == 2:  # Pedestrian
                    object_type = 6
                    type_ = "pedestrian"
                elif laser_label.type == 4:  # Cyclist
                    object_type = 5
                    type_ = "cyclist"
                list_types = [1, 2, 4]  # [1, 2, 4]
                # Don't write other classes in Waymo Open Dataset:
                if laser_label.type not in list_types:
                    continue

                bbox.centroid.x = laser_label.box.center_x
                bbox.centroid.y = laser_label.box.center_y
                bbox.centroid.z = laser_label.box.center_z

                q = Quaternion.from_euler(0, 0, laser_label.box.heading, degrees=False)
                q = q.normalize

                bbox.orientation.x = q.x
                bbox.orientation.y = q.y
                bbox.orientation.z = q.z
                bbox.orientation.w = q.w

                bbox.size.x = laser_label.box.length
                bbox.size.y = laser_label.box.width
                bbox.size.z = laser_label.box.height
                bbox.heading = laser_label.box.heading

                bbox.vehicle_label = object_type

                bboxArr.boxes.append(bbox)

                # Marker:
                marker_cube = self.ros_related.giveCubeLabel(frame_stamp_secs, frame_stamp_nsecs, ID, laser_label, frame_id, type_)
                marker_arrow = self.ros_related.giveArrowMarker(frame_stamp_secs, frame_stamp_nsecs, laser_label, ID, frame_id, type_)

                lidar_label_markerArr.markers.append(marker_cube)
                lidar_label_markerArr.markers.append(marker_arrow)


            timestamp_bag_nano = int(frame_stamp_secs * 1e9) + int(frame_stamp_nsecs)

            self.list_frame_by_frame_lidar_labels.append([lidar_label_markerArr, '/lidar_labels', timestamp_bag_nano])
            self.list_frame_by_frame_bbox_array.append([bboxArr, '/detected_objects_bbox_array', timestamp_bag_nano])

            # END: #########################################################################

            print(
                "Frame end:#############################################################################################")

        return self.list_frame_by_frame_lidar_labels, \
               self.list_frame_by_frame_trans, \
               self.list_frame_by_frame_odometry, \
               self.list_frame_by_frame_bbox_array, \
               len(list_frame_ids)
