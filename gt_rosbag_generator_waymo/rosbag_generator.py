import rosbag2_py
from rclpy.serialization import deserialize_message, serialize_message
from rosidl_runtime_py.utilities import get_message
import os
import sys

from os import listdir
from os.path import isfile, join
from Helper import Helper


def get_rosbag_options(path, serialization_format='cdr'):
    storage_options = rosbag2_py.StorageOptions(uri=path, storage_id='sqlite3')
    converter_options = rosbag2_py.ConverterOptions(
        input_serialization_format=serialization_format,
        output_serialization_format=serialization_format)
    return storage_options, converter_options


def create_topic(writer, topic_name, topic_type, serialization_format='cdr'):
    """
    Create a new topic.
    :param writer: writer instance
    :param topic_name:
    :param topic_type:
    :param serialization_format:
    :return:
    """
    topic_name = topic_name
    topic = rosbag2_py.TopicMetadata(name=topic_name, type=topic_type,
                                     serialization_format=serialization_format)
    writer.create_topic(topic)


if __name__ == '__main__':
    path_tf_folder = sys.argv[1]  # '/home/goktug/Desktop/Waymo_Valid_Dataset/valid0/validation_validation_0000'
    output_folder_path = sys.argv[2]  # '/home/goktug/ros2bags'


    list_name_tfrecords = [f for f in listdir(path_tf_folder) if isfile(join(path_tf_folder, f))]

    # Iterate over tf paths:
    for tf_name in list_name_tfrecords:
        path_ros2bag = output_folder_path + '/' + tf_name.split('.')[0]
        path_tfrecord = path_tf_folder + '/' + tf_name
        # Read tfrecord:
        helper = Helper(path_tfrecord)

        list_frame_by_frame_lidar_labels, \
        list_frame_by_frame_trans, \
        list_frame_by_frame_odometry, list_frame_by_frame_bbox_array,\
        frame_count = helper.readDataset()

        storage_options, converter_options = get_rosbag_options(path_ros2bag)
        writer = rosbag2_py.SequentialWriter()
        writer.open(storage_options, converter_options)

        # Iterate over frames:
        for frame_id in range(frame_count):
            single_frame_lidar_labels = list_frame_by_frame_lidar_labels[frame_id]
            single_frame_trans = list_frame_by_frame_trans[frame_id]

            single_frame_odometry = list_frame_by_frame_odometry[frame_id]
            single_frame_bbox_array = list_frame_by_frame_bbox_array[frame_id]

            # 0: msg, 1: topic_name, 2: time_stamp
            create_topic(writer, single_frame_lidar_labels[1], 'visualization_msgs/msg/MarkerArray')
            writer.write(single_frame_lidar_labels[1],
                         serialize_message(single_frame_lidar_labels[0]),
                         single_frame_lidar_labels[2])
            # Write tf msgs:
            create_topic(writer, single_frame_trans[1], 'tf2_msgs/msg/TFMessage')
            writer.write(single_frame_trans[1],
                         serialize_message(single_frame_trans[0]),
                         single_frame_trans[2])

            # Write odometry in map frame:
            create_topic(writer, single_frame_odometry[1], 'nav_msgs/msg/Odometry')
            writer.write(single_frame_odometry[1],
                         serialize_message(single_frame_odometry[0]),
                         single_frame_odometry[2])
            # Write bbox array:
            create_topic(writer, single_frame_bbox_array[1], 'autoware_auto_msgs/msg/BoundingBoxArray')
            writer.write(single_frame_bbox_array[1],
                         serialize_message(single_frame_bbox_array[0]),
                         single_frame_bbox_array[2])



        # break
        print(path_ros2bag, " DONE!")
        print('************************************************************************************************')

    print("All files are extracted.")
